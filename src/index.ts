import { TemplateResult, render } from 'lit-html';

export type Dispatcher<TMsg> = (msg: TMsg) => void;
// TODO the View type uses TemplateResult from lit-html, but it doesn't strictly
//  have to be bound to lit-html. What if this was just a string? Just an HTMLElement, react node... etc.
export type View<TModel, TMsg> = (model: TModel, dispatcher: Dispatcher<TMsg>) => TemplateResult;

/** The update function
 * 
 * This function, at its simplest, takes in the current state and the current message
 * and returns new state: model -> msg -> state
 * 
 * lit-tea allows you to return a tuple of [model, msg] to chain messages together.
 * lit-tea also allows you to return a promise of either state of [model, msg] to
 * asynchronously update the state or chain multiple async msgs together.
 */
export type Update<TModel, TMsg> = (model: TModel, msg: TMsg, dispatcher: Dispatcher<TMsg>) =>
  Promise<TModel> | Promise<[TModel, TMsg]> | TModel | [TModel, TMsg]

/** A convenient structure to package an entire SPA up into */
export interface Application<TModel, TMsg> {
  init: TModel,
  view: View<TModel, TMsg>,
  update: Update<TModel, TMsg>,
  /** Defaults to 'root' */
  appRootId?: string
}

// TODO this is the only part bound to lit-html, no reason this couldn't
//  be react, vue, web components... etc.
const renderTemplate = <TModel, TMsg>(view: View<TModel, TMsg>, model: TModel, dispatcher: Dispatcher<TMsg>, rootEl: HTMLElement) =>
  render(view(model, dispatcher), rootEl);

class AppState<TModel> {
  private currentState: TModel;

  constructor(init: TModel) {
    this.currentState = init;
  }

  getState = () =>
    this.currentState;

  setState(newState: TModel) {
    this.currentState = newState;
    return newState;
  }
}

/**
 * Kicks of rendering of a SPA, complete with reactive updates
 * of any messages the view dispatches.
 * 
 * @param app The application as defined by its Model, View, and Update.
 */
export const run = <TModel, TMsg>(app: Application<TModel, TMsg>) => {
  const appRoot = document.getElementById(app.appRootId || 'root');

  if (appRoot == null) {
    throw new Error('Can\'t find element by id with id: \'' + app.appRootId + '\'');
  }

  const appState = new AppState(app.init);

  const updatePipeline: Dispatcher<TMsg> = msg => {
    const currState = appState.getState();
    const updateReturn = app.update(currState, msg, updatePipeline);

    // lift to promise always
    Promise.resolve(updateReturn)
      .then(updated => {
        if (Array.isArray(updated)) {
          const [intermediateState, nextMsg] = updated;
          appState.setState(intermediateState);
          updatePipeline(nextMsg);
        } else {
          appState.setState(updated);
          rerender(updated);
        }
      })
  };

  const rerender = (state: TModel) => {
    renderTemplate(app.view, state, updatePipeline, appRoot);
  }

  renderTemplate(app.view, appState.getState(), updatePipeline, appRoot);

  return Promise.resolve('Application rendered');
}