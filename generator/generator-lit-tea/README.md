# generator-lit-tea
> Bare bones lit-tea spa

## Installation

First, install [Yeoman](http://yeoman.io) and generator-lit-tea using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-lit-tea
```

Then generate your new project:

```bash
yo lit-tea my-project-name
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

ISC © [Sean Newell](sean.thenewells.us)
