import { html } from 'lit-html';
import { run, View, Update } from 'lit-tea';


// MODEL - application state
type Model = {
  counter: number,
  amount: number
}

const init: Model = {
  counter: 0,
  amount: 1
};


// ACTIONS - application transitions
type SetAmount = {
  tag: 'Amount',
  value: number
}

type Increment = {
  tag: 'Inc'
}

type Decrement = {
  tag: 'Dec'
}

type Msg =
  SetAmount
  | Increment
  | Decrement

const mkAmntMsg = (amnt: number): SetAmount =>
  ({ tag: 'Amount', value: amnt })

const INC: Increment = { tag: 'Inc' };
const DEC: Decrement = { tag: 'Dec' };


// UPDATE - take action and state, do the thing
const update: Update<Model, Msg> = (model, msg, dispatcher) => {
  switch (msg.tag) {
    case 'Amount':
      return { ...model, amount: msg.value };
    case 'Dec':
      return { ...model, counter: model.counter - model.amount };
    case 'Inc':
      return { ...model, counter: model.counter + model.amount };
  }
}

const isInputEvent = (evt: any): evt is HTMLInputElement =>
  evt['value'];

const getValFromEvent = (evt: Event): number => {
  if (evt.currentTarget && isInputEvent(evt.currentTarget)) {
    return parseInt(evt.currentTarget.value || '1') || 1;
  }

  return 1;
}

// VIEW - dat sweet html
const view: View<Model, Msg> = (model, dispatcher) => {

  const inc = () =>
    dispatcher(INC);

  const dec = () =>
    dispatcher(DEC)

  const changeAmnt = (evt: Event) =>
    dispatcher(mkAmntMsg(getValFromEvent(evt)));

  return html`
    <section class="hero is-dark is-bold">
      <div class="hero-body">
        <div class="container">
          <h1 class="title">
            <%= name %>
          </h1>
          <h2 class="subtitle">
            Get Lit!
          </h2>
        </div>
      </div>
    </section>

    <section class="section">
      <p class="is-size-2">${model.counter}</p>
      <button class="button is-primary" @click=${inc}>Increment</button>
      <button class="button is-primary" @click=${dec}>Decrement</button>
      <input class="input" @keyup=${changeAmnt} type="number" value=${model.amount} />
    </section>
  `;
}


// APP - tyingit all together
run({ init, update, view });
