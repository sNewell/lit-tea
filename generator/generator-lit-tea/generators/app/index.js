"use strict";
const Generator = require("yeoman-generator");
const chalk = require("chalk");
const yosay = require("yosay");

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    this.argument("name", {
      default: "lit-app",
      type: String,
      description: "Name of your new app, defaults to lit-app"
    });
  }

  prompting() {
    this.log(
      yosay(`Welcome to the rad ${chalk.red("generator-lit-tea")} generator!`)
    );

    this.log(
      `  > building a new lit-tea app in ${chalk.blue(
        this.destinationRoot()
      )} title ${chalk.green(this.options.name)}`
    );
  }

  writing() {
    const samplePackageJson = {
      scripts: {
        start: "parcel index.html",
        build: "parcel build index.hml"
      },
      dependencies: {
        "lit-html": "~1.1.2",
        "lit-tea": "0.0.2-pre",
        parcel: "~1.12.4",
        bulma: "~0.8.0"
      },
      devDependencies: {
        typescript: "~3.6.4"
      }
    };

    const opts = { name: this.options.name };

    this.fs.extendJSON(this.destinationPath("package.json"), samplePackageJson);
    this.fs.copyTpl(
      this.templatePath("tsconfig.json"),
      this.destinationPath("tsconfig.json")
    );
    this.fs.copyTpl(
      this.templatePath("index.html"),
      this.destinationPath("index.html"),
      opts
    );
    this.fs.copyTpl(
      this.templatePath("index.ts"),
      this.destinationPath("src/index.ts"),
      opts
    );
  }

  install() {
    this.installDependencies();
  }
};
