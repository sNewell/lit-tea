import { html } from 'lit-html';
import { run, View, Update } from '../src'


// MODEL - application state
type Model = {
  saying: string,
  itrs: number
}

const init: Model = {
  saying: 'hello',
  itrs: 50
};


// MESSAGES - application transitions
type NewSaying = {
  tag: 'NewSaying',
  value: string
}

const mkNewSaying = (newSaying: string): NewSaying => ({
  tag: 'NewSaying',
  value: newSaying
});

type Noop = {
  tag: 'Noop'
}

type Msg =
  NewSaying
  | Noop


// UPDATE - transform the model with the incoming message
const update: Update<Model, Msg> = (model, msg) => {
  switch (msg.tag) {
    case "NewSaying":
      return { ...model, saying: msg.value};
    case "Noop":
      return model;
  }
}


// VIEW - dat sweet html
const view: View<Model, Msg> = (model, dispatcher) => {

  const nextSaying = model.saying === 'hello' ? 'ahoy' : 'hello';

  const onBtnClick = () =>
    dispatcher(mkNewSaying(nextSaying));

  return html`
    <section>
      <p>Well, it is ${model.saying}</p>
      <button @click=${onBtnClick}>Change it</button>
    </section>
  `;
}


// APP - tyingit all together
run({ init, update, view });
