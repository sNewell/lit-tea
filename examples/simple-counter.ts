import { run, Dispatcher } from '../src';
import { html } from 'lit-html';

type Msg = { incr: number }

const INC: Msg = { incr: 1 };
const DEC: Msg = { incr: -1 };

type Model = {
  counter: number
}

type MsgHandler = (d: Dispatcher<Msg>) => void;

const increment: MsgHandler = dispatch => dispatch(INC);
const decrement: MsgHandler = dispatch => dispatch(DEC);

run<Model, Msg>({
  init: { counter: 0 },
  update: (model, msg) => ({
    ...model,
    counter: model.counter + msg.incr
  }),
  view: (model, dispatch) =>
    html`
      <h2>${model.counter}</h2>
      <div>
        <button @click=${increment(dispatch)}>+1</button>
        <button @click=${decrement(dispatch)}>-1</button>
      </div>
    `
});
