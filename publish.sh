#!/bin/sh

# build the typescript files
npm run build

# copy all the stuffs
cp readme.md package.json dist/
cd dist/

# run npm publish in the dist folder
npm publish
cd ..
